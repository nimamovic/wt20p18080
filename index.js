const express = require("express");
const session = require("express-session");
const bodyParser = require("body-parser");
const fs = require('fs');

const { Op } = require("sequelize");
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }))
app.use(express.static('js'));
app.use(express.static('css'));

app.use("/css", express.static(__dirname + '/css'));

const db = require('./db.js');
db.sequelize.sync({force:true}).then(function(){
    inicijalizacija();
    console.log("Kreirana baza");
});
const { Grupa, Student } = require("./db.js");



app.post("/v2/predmet",async (req, res) => {
    try{
    const PredmetModel = db.Predmet;
    const PronadjiPredmet = await PredmetModel.findOne({where:{naziv:req.body.naziv}});
    if (!PronadjiPredmet) {
            const predmetDetails = await PredmetModel.create({naziv:req.body.naziv});
            if(!predmetDetails){
                return res.status(200).send({
                  status: 404,
                  message: 'No data found'
             });
             }
    }   
    if(PronadjiPredmet){
        res.status(200).send({
        status: 404,
        message: 'Predmet sa ovim imenom već postoji'
     });
    }
    else {
        res.status(200).send({
            status: 200,
            message: 'Predmet spremljen u bazu'
         });
    }
    }catch(error){
        console.log(error)
        return res.status(400).send({
          message:'Unable to find data',
          errors: error,
          status: 400
     });
     }
});

app.get("/v2/predmet",async (req, res) => {
    try{
    const PredmetModel = db.Predmet;
    const predmetDetails  = await PredmetModel.findAll();
    if(!predmetDetails){
        return res.status(200).send({
        status: 404,
        message: 'No data found'
    });
    }
    res.json(predmetDetails);
    }
    catch(error){
        console.log(error)
        return res.status(400).send({
          message:'Unable to find data',
          errors: error,
          status: 400
     });
     }
 });
app.get("/v2/predmet/:id",async (req, res) => {
    const id = req.params.id;
    try{
    const PredmetModel = db.Predmet;
    const predmetDetails  = await PredmetModel.findOne(
        {where: {id: id} }
    );
    if(!predmetDetails){
        return res.status(200).send({
        status: 404,
        message: 'No data found'
    });
    }
    res.json(predmetDetails);
    }
    catch(error){
        console.log(error)
        return res.status(400).send({
          message:'Unable to find data',
          errors: error,
          status: 400
     });
     }
 });

app.put("/v2/predmet/:id", async (req, res) => {
    try{
    const id = req.params.id;
    const PredmetModel = db.Predmet;
    const PronadjiPredmet = await PredmetModel.findOne({
        where:{
        naziv:req.body.naziv,
        [Op.not]: [
        { id: id }
    ]}});
    if (!PronadjiPredmet) {
        const predmetDetails = await PredmetModel.update({
            naziv: req.body.naziv,
        },
        {where: {id: id} });
            if(!predmetDetails){
                return res.status(200).send({
                  status: 404,
                  message: 'No data found'
             });
             }
    }   
    if(PronadjiPredmet){
        res.status(200).send({
        status: 404,
        message: 'Predmet sa ovim imenom već postoji'
     });
    }
    else {
        res.status(200).send({
            status: 200,
            message: 'Predmet editovan'
         });
    }
    }
    catch(error){
        console.log(error)
        return res.status(400).send({
        message:'Unable to find data',
        errors: error,
        status: 400
    });
    }
 });

 app.delete("/v2/predmet/:id", async (req, res) => {
    try{
    const id = req.params.id;
    const PredmetModel = db.Predmet;
    const predmetDetails = await PredmetModel.destroy({
    where: { id: id }
    });
    if(!predmetDetails){
    return res.status(200).send({
      status: 404,
      message: 'No data found'
    });
    }
    res.status(200).send({
        status: 200,
        message: 'Data Delete Successfully'
    });
    }
    catch(error){
        console.log(error)
        return res.status(400).send({
          message:'Unable to find data',
          errors: error,
          status: 400
     });
     }
 });
 app.post("/v2/dan",async (req, res) => {
    try{
        const DanModel = db.Dan;
        const PronadjiDan = await DanModel.findOne({where:{naziv:req.body.naziv}});
        if (!PronadjiDan) {
                const danDetails  = await DanModel.build({
                    naziv: req.body.naziv,
                 });
                await danDetails.save();
                if(!danDetails){
                    return res.status(200).send({
                    status: 404,
                    message: 'No data found'
                });
                }
        }   
        if(PronadjiDan){
            res.status(200).send({
            status: 404,
            message: 'Ovaj dan već postoji u bazi'
        });
        }
        else {
            res.status(200).send({
                status: 200,
                message: 'Dan spremljen u bazu'
            });
        }
    }
    catch(error){
        console.log(error)
        return res.status(400).send({
          message:'Unable to find data',
          errors: error,
          status: 400
     });
     }
});

app.get("/v2/dan",async (req, res) => {
    try{
    const DanModel = db.Dan;
    const danDetails  = await DanModel.findAll();
    if(!danDetails){
        return res.status(200).send({
        status: 404,
        message: 'No data found'
    });
    }
    res.json(danDetails);
    }
    catch(error){
        console.log(error)
        return res.status(400).send({
          message:'Unable to find data',
          errors: error,
          status: 400
     });
     }
 });
 app.get("/v2/dan/:id",async (req, res) => {
    const id = req.params.id;
    try{
    const DanModel = db.Dan;
    const danDetails  = await DanModel.findOne({where: {id: id}});
    if(!danDetails){
        return res.status(200).send({
        status: 404,
        message: 'No data found'
    });
    }
    res.json(danDetails);
    }
    catch(error){
        console.log(error)
        return res.status(400).send({
          message:'Unable to find data',
          errors: error,
          status: 400
     });
     }
 });
app.put("/v2/dan/:id", async (req, res) => {
    const id = req.params.id;
    const DanModel = db.Dan;
        const PronadjiDan = await DanModel.findOne({where:{naziv:req.body.naziv,[Op.not]: [
            { id: id }]
        }});
        if (!PronadjiDan) {
            const danDetails = await DanModel.update({
                naziv: req.body.naziv,
               },
              {where: {id: id} });
                if(!danDetails){
                    return res.status(200).send({
                    status: 404,
                    message: 'No data found'
                });
                }
        }   
        if(PronadjiDan){
            res.status(200).send({
            status: 404,
            message: 'Ovaj dan već postoji u bazi'
        });
        }
        else {
            res.status(200).send({
                status: 200,
                message: 'Dan spremljen u bazu'
            });
        }
 });

 app.delete("/v2/dan/:id", async (req, res) => {
    const id = req.params.id;
    const DanModel = db.Dan;
    const danDetails = await DanModel.destroy({
    where: { id: id }
    });
    if(!danDetails){
    return res.status(200).send({
      status: 404,
      message: 'No data found'
    });
    }
    res.status(200).send({
        status: 200,
        message: 'Data Delete Successfully'
    });
 });
 app.post("/v2/grupa",async (req, res) => {
    try
    {const GrupaModel = db.Grupa;
    const grupaDetails  = await GrupaModel.build({
          naziv: req.body.naziv,
          predmet: req.body.predmet,
     });
     await grupaDetails.save()
     if(!grupaDetails){
        return res.status(200).send({
          status: 404,
          message: 'No data found'
     });
     }
     res.status(200).send({
        status: 200,
        message: 'Data Save Successfully'
     });
    }
    catch(error){
        console.log(error)
        return res.status(400).send({
          message:'Unable to find data',
          errors: error,
          status: 400
     });
     }
});

app.get("/v2/grupa",async (req, res) => {
    try{
    const GrupaModel = db.Grupa;
    const grupaDetails  = await GrupaModel.findAll();
    if(!grupaDetails){
        return res.status(200).send({
        status: 404,
        message: 'No data found'
    });
    }
    res.json(grupaDetails);
    }
    catch(error){
        console.log(error)
        return res.status(400).send({
          message:'Unable to find data',
          errors: error,
          status: 400
     });
     }
 });
app.get("/v2/grupa/:id",async (req, res) => {
    const id = req.params.id;
    try{
    const GrupaModel = db.Grupa;
    const grupaDetails  = await GrupaModel.findOne({where: {id: id}});
    if(!grupaDetails){
        return res.status(200).send({
        status: 404,
        message: 'No data found'
    });
    }
    res.json(grupaDetails);
    }
    catch(error){
        console.log(error)
        return res.status(400).send({
          message:'Unable to find data',
          errors: error,
          status: 400
     });
     }
 });

app.put("/v2/grupa/:id", async (req, res) => {
    try{
    const id = req.params.id;
    const GrupaModel = db.Grupa;
    const grupaDetails = await GrupaModel.update({
      naziv: req.body.naziv,
      predmet: req.body.predmet,
     },
    {where: {id: id} });
    if(!grupaDetails){
        return res.status(200).send({
        status: 404,
        message: 'No data found'
    });
    }
    res.status(200).send({
        status: 200,
        message: 'Data Update Successfully'
    });
    }
    catch(error){
        console.log(error)
        return res.status(400).send({
          message:'Unable to find data',
          errors: error,
          status: 400
     });
     }
 });

 app.delete("/v2/grupa/:id", async (req, res) => {
    try{
    const id = req.params.id;
    const GrupaModel = db.Grupa;
    const grupaDetails = await GrupaModel.destroy({
    where: { id: id }
    });
    if(!grupaDetails){
    return res.status(200).send({
      status: 404,
      message: 'No data found'
    });
    }
    res.status(200).send({
        status: 200,
        message: 'Data Delete Successfully'
    });
    }
    catch(error){
        console.log(error)
        return res.status(400).send({
          message:'Unable to find data',
          errors: error,
          status: 400
     });
     }
 });
 app.post("/v2/tip",async (req, res) => {
    try{
        const TipModel = db.Tip;
        const PronadjiTip = await TipModel.findOne({where:{naziv:req.body.naziv}});
        if (!PronadjiTip) {
                const tipDetails  = await TipModel.build({
                    naziv: req.body.naziv,
               });
               await tipDetails.save();
                if(!tipDetails){
                    return res.status(200).send({
                    status: 404,
                    message: 'No data found'
                });
                }
        }   
        if(PronadjiTip){
            res.status(200).send({
            status: 404,
            message: 'Ovaj tip već postoji u bazi'
        });
        }
        else {
            res.status(200).send({
                status: 200,
                message: 'Tip spremljen u bazu'
            });
        }
    }
    catch(error){
        console.log(error)
        return res.status(400).send({
          message:'Unable to find data',
          errors: error,
          status: 400
     });
     }
});

app.get("/v2/tip",async (req, res) => {
    try{
    const TipModel = db.Tip;
    const tipDetails  = await TipModel.findAll();
    if(!tipDetails){
        return res.status(200).send({
        status: 404,
        message: 'No data found'
    });
    }
    res.json(tipDetails);
    }
    catch(error){
        console.log(error)
        return res.status(400).send({
          message:'Unable to find data',
          errors: error,
          status: 400
     });
     }
 });

app.get("/v2/tip/:id",async (req, res) => {
    const id = req.params.id;
    try{
    const TipModel = db.Tip;
    const tipDetails  = await TipModel.findOne({where: {id: id}});
    if(!tipDetails){
        return res.status(200).send({
        status: 404,
        message: 'No data found'
    });
    }
    res.json(tipDetails);
    }
    catch(error){
        console.log(error)
        return res.status(400).send({
          message:'Unable to find data',
          errors: error,
          status: 400
     });
     }
 });


app.put("/v2/tip/:id", async (req, res) => {
    try{
    const id = req.params.id;
    const TipModel = db.Tip;
        const PronadjiTip = await TipModel.findOne({where:{naziv:req.body.naziv,[Op.not]: [
            { id: id }]}});
        if (!PronadjiTip) {
            const tipDetails = await TipModel.update({
                naziv: req.body.naziv,
               },
              {where: {id: id} });
                if(!tipDetails){
                    return res.status(200).send({
                    status: 404,
                    message: 'No data found'
                });
                }
        }   
        if(PronadjiTip){
            res.status(200).send({
            status: 404,
            message: 'Ovaj tip već postoji u bazi'
        });
        }
        else {
            res.status(200).send({
                status: 200,
                message: 'Tip spremljen u bazu'
            });
        }
    }
    catch(error){
        console.log(error)
        return res.status(400).send({
          message:'Unable to find data',
          errors: error,
          status: 400
     });
     }
 });

 app.delete("/v2/tip/:id", async (req, res) => {
     try{
    const id = req.params.id;
    const TipModel = db.Tip;
    const tipDetails = await TipModel.destroy({
    where: { id: id }
    });
    if(!tipDetails){
    return res.status(200).send({
      status: 404,
      message: 'No data found'
    });
    }
    res.status(200).send({
        status: 200,
        message: 'Data Delete Successfully'
    });
    }
    catch(error){
        console.log(error)
        return res.status(400).send({
          message:'Unable to find data',
          errors: error,
          status: 400
     });
     }
 });

 app.post("/v2/studente",async (req, res) => {
    let niz = req.body.studenti;
    let grupa = req.body.grupa;
    let statusM = [];
    const StudentModel = db.Student;
    const GrupaModel = db.Grupa;
    try{
    const GrupaDodavanje = await GrupaModel.findOne({
        where: { naziv: grupa },
      });
    for(var i = 0; i < niz.length; i++){
        const sviStudenti  = await StudentModel.findAll(
            { where:{
                ime: niz[i].ime,
                index: niz[i].index,
            }
            }
        );
        const sviStudentiSaIstimIndeksom = await StudentModel.findAll(
            { where:{
                index: niz[i].index,
            }
            }
        );
        console.log("udje" + sviStudentiSaIstimIndeksom.length);
        if(sviStudenti.length == 0 && sviStudentiSaIstimIndeksom.length == 0){
            const studentDetails = await db.Student.create(
                {
                    ime: niz[i].ime,
                    index: niz[i].index,
                }
            );
            await studentDetails.addGrupe(GrupaDodavanje);

        }
        else if(sviStudenti.length != 0){
           const GrupaNova = await db.Grupa.findAll({
                where: {
                    predmet: GrupaDodavanje.predmet,
                },
                include:
                {
                    model: StudentModel,
                    as: 'studenti',
                    through:'student_grupa'
                }
            });
            //nmg sad da testiram ali je mozda pametnije staviti > 1 pokusacu
            if(GrupaNova.length != 1){
                await sviStudenti[0].removeGrupe(GrupaNova);
                await sviStudenti[0].addGrupe(GrupaDodavanje);
            }
            else await sviStudenti[0].addGrupe(GrupaDodavanje);

        }
        else if(sviStudentiSaIstimIndeksom.length != 0){
            statusM.push('Student ' + niz[i].ime +  ' nije kreiran jer postoji student ' +  sviStudentiSaIstimIndeksom[0].ime + ' sa istim indexom ' + niz[i].index);
            
        }
    }
        if(niz.length == 0){
                return res.status(200).send({
                status: 404,
                message: 'No data found'
            });
            }
        else{
            if(statusM.length == 0){
                res.status(200).send({
                    status: 200,
                    message: statusM
                });
            }
            if(statusM.length != 0){
                return res.status(200).send({
                    message: statusM
                });
            }
        }
    }
        catch(error){
            console.log(error)
            return res.status(400).send({
              message:'Unable to find data',
              errors: error,
              status: 400
         });
         }
    
});

app.post("/v2/student",async (req, res) => {
    try{
    const StudentModel = db.Student;
    const PronadjiStudent= await StudentModel.findOne({where:{index:req.body.index}});
        if (!PronadjiStudent) {
            const StudentDetails = await StudentModel.create({
                ime: req.body.ime,
                index: req.body.index,
            });
                if(!StudentDetails){
                    return res.status(200).send({
                      status: 404,
                      message: 'No data found'
                 });
                 }
        }   
        if(PronadjiStudent){
            res.status(200).send({
            status: 404,
            message: 'Student sa ovim indexom već postoji'
         });
        }
        else {
            res.status(200).send({
                status: 200,
                message: 'Student editovan'
             });
        } 
   }
   catch(error){
       console.log(error)
       return res.status(400).send({
         message:'Unable to find data',
         errors: error,
         status: 400
    });
    }
});


app.get("/v2/student/:id",async (req, res) => {
    const id = req.params.id;
    try{
    const StudentModel = db.Student;
    const studentDetails  = await StudentModel.findOne({where: {id: id}});
    if(!studentDetails){
        return res.status(200).send({
        status: 404,
        message: 'No data found'
    });
    }
    res.json(studentDetails);
    }
    catch(error){
        console.log(error)
        return res.status(400).send({
          message:'Unable to find data',
          errors: error,
          status: 400
     });
     }
 });
 app.get("/v2/student",async (req, res) => {
    try{
    const StudentModel = db.Student;
    const studentDetails  = await StudentModel.findAll();
    if(!studentDetails){
        return res.status(200).send({
        status: 404,
        message: 'No data found'
    });
    }
    res.json(studentDetails);
    }
    catch(error){
        console.log(error)
        return res.status(400).send({
          message:'Unable to find data',
          errors: error,
          status: 400
     });
     }
 });

app.put("/v2/student/:id", async (req, res) => {
    try{
    const id = req.params.id;
    const StudentModel = db.Student;
    const PronadjiStudent= await StudentModel.findOne({where:{index:req.body.index,
        [Op.not]: [
            { id: id }
        ]}});
        if (!PronadjiStudent) {
            const StudentDetails = await StudentModel.update({
                ime: req.body.ime,
                index: req.body.index,
               },
              {where: {id: id} });
                if(!StudentDetails){
                    return res.status(200).send({
                      status: 404,
                      message: 'No data found'
                 });
                 }
        }   
        if(PronadjiStudent){
            res.status(200).send({
            status: 404,
            message: 'Student sa ovim indexom već postoji'
         });
        }
        else {
            res.status(200).send({
                status: 200,
                message: 'Student editovan'
             });
        }
    }
    catch(error){
        console.log(error)
        return res.status(400).send({
          message:'Unable to find data',
          errors: error,
          status: 400
     });
     }
 });

 app.delete("/v2/student/:id", async (req, res) => {
     try{
    const id = req.params.id;
    const StudentModel = db.Student;
    const studentDetails = await StudentModel.destroy({
    where: { id: id }
    });
    if(!studentDetails){
    return res.status(200).send({
      status: 404,
      message: 'No data found'
    });
    }
    res.status(200).send({
        status: 200,
        message: 'Data Delete Successfully'
    });
    }
    catch(error){
        console.log(error)
        return res.status(400).send({
          message:'Unable to find data',
          errors: error,
          status: 400
     });
     }

 });

 app.post("/v2/aktivnost",async (req, res) => {
     try{
    const AktivnostModel = db.Aktivnost;
    const aktivnostDetails  = await AktivnostModel.build({
          naziv: req.body.naziv,
          pocetak: req.body.pocetak,
          kraj: req.body.kraj,
          predmet: req.body.predmet,
          grupa: req.body.grupa,
          dan: req.body.dan,
          tip: req.body.tip,
     });
     await aktivnostDetails.save()
     if(!aktivnostDetails){
        return res.status(200).send({
          status: 404,
          message: 'No data found'
     });
     }
     res.status(200).send({
        status: 200,
        message: 'Data Save Successfully'
     });
    }
    catch(error){
        console.log(error)
        return res.status(400).send({
          message:'Unable to find data',
          errors: error,
          status: 400
     });
     }
});

app.post("/v2/aktivnost/unos",async (req, res) => {
    let nazivAkt = req.body.naziv + req.body.tip;
    let nazivN = req.body.naziv;
    let tip = req.body.tip;
    let pocetak = req.body.pocetak;
    let kraj = req.body.kraj;
    pocetak = pocetak.replace(":00","");
    pocetak = pocetak.replace(":30",".5");
    pocetak = pocetak.replace("10","1x");
    pocetak = pocetak.replace("0","");
    pocetak = pocetak.replace("1x","10");
    kraj = kraj.replace(":00","");
    kraj = kraj.replace(":30",".5");
    kraj = kraj.replace("10","1x");
    kraj = kraj.replace("0","");
    kraj = kraj.replace("1x","10");
    if(parseFloat(pocetak).toString() != pocetak || parseFloat(kraj).toString() != kraj || parseFloat(pocetak) < 8 || parseFloat(pocetak) > 20 || parseFloat(kraj) < 8 || parseFloat(kraj) > 20 || parseFloat(kraj) < parseFloat(pocetak)){
        res.status(200).send({
            status: 404,
            message: 'Neispravan format vremena'
         });
       return;
    }
    pocetak = parseFloat(pocetak);
    kraj = parseFloat(kraj);
    let dan = req.body.dan;
    console.log(tip);
    console.log(dan);
    try{
        const Tip = await db.Tip.findOne({
            where: { naziv: tip },
        });
        const Dan = await db.Dan.findOne({
            where: { naziv: dan },
        });
        const AktivnostModel = db.Aktivnost;
        const PronadjiAktivnosti2 = await AktivnostModel.findOne({
            where:{
                dan: Dan.id,
                [Op.or]:[
                   {pocetak: {[Op.lt]: pocetak},
                    kraj:  {[Op.gt]: kraj}
                    },
                    {pocetak: {[Op.lt]: pocetak},
                    kraj:  {[Op.gt]: pocetak}
                    },
                    {pocetak: {[Op.between]: [pocetak,kraj]}},
                    {pocetak: {[Op.gt]: pocetak},
                    kraj:  {[Op.lt]: kraj}
                    },
                    {pocetak: pocetak},
                    {kraj:kraj}    
                ]
            }});
        if(!PronadjiAktivnosti2){
            const PredmetModel = db.Predmet;
            let predmetId;
            const PronadjiPredmet = await PredmetModel.findOne({where:{naziv:nazivN}});
            if (!PronadjiPredmet) {
                const PredmetNovi = await PredmetModel.create({naziv:nazivN});
                predmetId = PredmetNovi.id;
            }
            else predmetId = PronadjiPredmet.id;
            const aktivnostDetails  = await AktivnostModel.build({
            naziv: nazivAkt,
            pocetak: pocetak,
            kraj: kraj,
            predmet: predmetId,
            dan: Dan.id,
            tip: Tip.id,
            });
            await aktivnostDetails.save()
            if(!aktivnostDetails){
            return res.status(200).send({
                status: 404,
                message: 'No data found'
            });
            }
            res.status(200).send({
                status: 200,
                message: 'Data Save Successfully'
            });
        }
        else {
            res.status(200).send({
                status: 404,
                message: 'Aktivnost nije validna'
            });
        } 
   }
   catch(error){
       console.log(error)
       return res.status(400).send({
         message:'Unable to find data',
         errors: error,
         status: 400
    });
    }
});

app.get("/v2/aktivnost",async (req, res) => {
    try{
    const AktivnostModel = db.Aktivnost;
    const aktivnostDetails  = await AktivnostModel.findAll();
    if(!aktivnostDetails){
        return res.status(200).send({
        status: 404,
        message: 'No data found'
    });
    }
    res.json(aktivnostDetails); 
    }
    catch(error){
        console.log(error)
        return res.status(400).send({
          message:'Unable to find data',
          errors: error,
          status: 400
     });
     }
 });
 app.get("/v2/aktivnost/:id",async (req, res) => {
     const id = req.params.id;
    try{
    const AktivnostModel = db.Aktivnost;
    const aktivnostDetails  = await AktivnostModel.findOne({where: {id: id}});
    if(!aktivnostDetails){
        return res.status(200).send({
        status: 404,
        message: 'No data found'
    });
    }
    res.json(aktivnostDetails); 
    }
    catch(error){
        console.log(error)
        return res.status(400).send({
          message:'Unable to find data',
          errors: error,
          status: 400
     });
     }
 });

app.put("/v2/aktivnost/:id", async (req, res) => {
    try{
    const id = req.params.id;
    const AktivnostModel = db.Aktivnost;
    const aktivnostDetails = await AktivnostModel.update({
        naziv: req.body.naziv,
        pocetak: req.body.pocetak,
        kraj: req.body.kraj,
        predmet: req.body.predmet,
        grupa: req.body.grupa,
        dan: req.body.dan,
        tip: req.body.tip,
     },
    {where: {id: id} });
    if(!aktivnostDetails){
        return res.status(200).send({
        status: 404,
        message: 'No data found'
    });
    }
    res.status(200).send({
        status: 200,
        message: 'Data Update Successfully'
    });
    }
    catch(error){
        console.log(error)
        return res.status(400).send({
          message:'Unable to find data',
          errors: error,
          status: 400
     });
     }
 });

 app.delete("/v2/aktivnost/:id", async (req, res) => {
     try{
    const id = req.params.id;
    const AktivnostModel = db.Aktivnost;
    const aktivnostDetails = await AktivnostModel.destroy({
    where: { id: id }
    });
    if(!aktivnostDetails){
    return res.status(200).send({
      status: 404,
      message: 'No data found'
    });
    }
    res.status(200).send({
        status: 200,
        message: 'Data Delete Successfully'
    });
    }
    catch(error){
        console.log(error)
        return res.status(400).send({
          message:'Unable to find data',
          errors: error,
          status: 400
     });
     }
 });

app.get("/spirala2rasporedi.html", function (req, res) {
    res.sendFile(__dirname + "/spirala2rasporedi.html");
});

app.get('/',function(req,res){
    res.sendFile(__dirname+"/spirala2rasporedi.html");
 });

 app.get("/studenti.html", function (req, res) {
    res.sendFile(__dirname + "/studenti.html");
});

app.get("/aktivnosti.html", function (req, res) {
    res.sendFile(__dirname + "/aktivnosti.html");
});

app.get("/unosRasporeda.html", function (req, res) {
    res.sendFile(__dirname + "/unosRasporeda.html");
});
app.get("/unos.js", function (req, res) {
    res.sendFile(__dirname + "/unos.js");
});
app.get("/pozivi.js", function (req, res) {
    res.sendFile(__dirname + "/pozivi.js");
});
app.get("/studenti.js", function (req, res) {
    res.sendFile(__dirname + "/studenti.js");
});
app.get("/studenti2.js", function (req, res) {
    res.sendFile(__dirname + "/studenti.js");
});
app.get("/index.js", function (req, res) {
    res.sendFile(__dirname + "/index.js");
});
app.get("/iscrtaj.js", function (req, res) {
    res.sendFile(__dirname + "/iscrtaj.js");
});

app.get("/planiranjeNastavnik.html", function (req, res) {
    res.sendFile(__dirname + "/planiranjeNastavnik.html");
});

app.get("/podaciStudent.html", function (req, res) {
    res.sendFile(__dirname + "/podaciStudent.html");
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.urlencoded({extended: true}));

function pojednostavi(str){
    var linije = str.split("\n");
    var result = [];
    var headers = linije[0].split(",");
    var duzina = headers.length;
    let i = 1;
    if(headers[0] != "naziv") {
        headers[0] = "naziv";
        headers[1] = "tip";
        headers[2] = "pocetak";
        headers[3] = "kraj";
        headers[4] = "dan";
        i = 0;
    }
    for(;i<linije.length;i++){
        var obj = {};
        var trenutnaLinija = linije[i].split(",");
        for(var j = 0;j < duzina;j++){
            obj[headers[j]] = trenutnaLinija[j];
        }
        console.log(headers.length + "duzina");
        if(duzina != 1){
            obj.pocetak = parseFloat(obj.pocetak);
            obj.kraj = parseFloat(obj.kraj);
        }
        console.log(obj.pocetak + "poc," + obj.kraj);
        result.push(obj);
    }
    return result; 
  }

app.get('/v1/predmeti',function(req,res){
   fs.readFile('predmeti.txt', function read(err, data) {
        if (err) {
            throw err;
        }
        var obj = pojednostavi(data.toString());
        res.json(obj);
    });
});

app.get('/v1/aktivnosti',function(req,res){
    fs.readFile('aktivnosti.txt', function read(err, data) {
         if (err) {
             throw err;
         }
         var obj = pojednostavi(data.toString());
         res.json(obj);
     });
 });

 app.get('/v1/predmet/:naziv/aktivnost/',function(req,res){
    fs.readFile('aktivnosti.txt', function read(err, data) {
         if (err) {
             throw err;
         }
         var aktivnosti  = pojednostavi(data.toString());
         var obj = [];
         for(var i = 0; i < aktivnosti.length; i++){
            if(aktivnosti[i].naziv == req.params.naziv) obj.push(aktivnosti[i]);
        }
        res.json(obj);
     });
 });

app.post('/v1/predmet',function(req,res){
    var text = fs.readFileSync('predmeti.txt','utf8');
    let obj = pojednostavi(text);
    let tijelo = req.body;
    console.log(req.body + "rekvest bodyyyy");
    let novaLinija = "\n"+tijelo['naziv'];
    let imaPredmet = 0;
    for(var i = 0; i < obj.length; i++){
        if(obj[i].naziv == tijelo['naziv']) imaPredmet = 1;
    }
    if(imaPredmet == 0 && tijelo['naziv'] != "") {
        fs.appendFile('predmeti.txt', novaLinija,function(err){
        if(err) throw err;
        res.json({message:"Uspješno dodan predmet!"});
    });
    }
    else {
        res.json({message:"Naziv predmeta postoji!"});
    }
 });

 app.post('/v1/aktivnost',function(req,res){
    let text = fs.readFileSync('aktivnosti.txt','utf8');
    let obj = pojednostavi(text);
    let text2 = fs.readFileSync('predmeti.txt','utf8');
    let obj2 = pojednostavi(text2);
    let tijelo = req.body;
    let imaPredmet = 0;
    let validno = 1;
    const pocetak = Number(tijelo['pocetak']);
    const kraj = Number(tijelo['kraj']);
    if(pocetak < 8 || pocetak > 20 || kraj > 20 || kraj < 8 || pocetak > kraj || kraj * 2 != parseInt(kraj * 2) || pocetak * 2 != parseInt(pocetak * 2)) validno = 0;
    /*for(var i = 0; i < obj2.length; i++){
        if(obj2[i].naziv == tijelo['naziv']) imaPredmet = 1;
    }*/
    for(var i = 0; i < obj.length; i++){
        //if(obj[i].naziv == tijelo['naziv']) validno = 0;
        if(obj[i].dan == tijelo['dan']){
            console.log(typeof pocetak + " m" + typeof Number(obj[i].pocetak) + " m" + kraj + " m" + Number(obj[i].kraj));
            if(pocetak > Number(obj[i].pocetak) && kraj < Number(obj[i].kraj)) validno = 0;
            if(pocetak > Number(obj[i].pocetak) && pocetak < Number(obj[i].kraj)) validno = 0;
            if(pocetak < Number(obj[i].pocetak) &&  kraj > Number(obj[i].pocetak)) validno = 0;
            if(pocetak < Number(obj[i].pocetak) &&  kraj > Number(obj[i].kraj)) validno = 0;
            if(pocetak ==  Number(obj[i].pocetak) || kraj ==  Number(obj[i].kraj)) validno = 0;
        }
    }
    if(validno == 1 && tijelo['naziv'] != "") {
        let novaLinija = "\n"+tijelo['naziv']+","+tijelo['tip']+
        ","+tijelo['pocetak']+","+tijelo['kraj']+","+tijelo['dan'];
        fs.appendFile('aktivnosti.txt',novaLinija,function(err){
            if(err) throw err;
            res.json({message:"Uspješno dodana aktivnost!"});
        });
    }
    else res.json({message:"Aktivnost nije validna!"}); 
 });
 app.delete('/v1/aktivnost/:naziv', function (req, res) { 
    var naziv = req.params.naziv;
    fs.readFile('aktivnosti.txt', function read(err, data) {
        if (err) {
            throw err;
        }
        let imaAkt = 0;
        let brojac = [];
        let vise = 0;
        let k = 0;
        var obj = pojednostavi(data.toString());
        for(var i = 0; i < obj.length; i++){
            console.log(obj[i].naziv);
            if(obj[i].naziv == naziv) {
            imaAkt = 1;
            brojac[k] = i;
            k++;
            vise++;
            }
        }
        let zadnji = 0;
        console.log(vise + "vise");
        if(imaAkt != 1)  res.json({message:"Greška - aktivnost nije obrisana!"});
        else{
            var sviPredmeti = fs.readFileSync('aktivnosti.txt', 'utf-8');
                if(vise == 1) if(obj[obj.length - 1].naziv == naziv) zadnji = 1;
                if(zadnji != 1)  {
                    console.log("udje");
                        for(k = 0; k < brojac.length; k++){
                        if(k != brojac.length - 1 || obj[obj.length - 1].naziv != naziv) sviPredmeti = sviPredmeti.replace(`${obj[brojac[k]].naziv},${obj[brojac[k]].tip},${obj[brojac[k]].pocetak},${obj[brojac[k]].kraj},${obj[brojac[k]].dan}\n`,'');
                        var zavrsni = sviPredmeti;
                            if(k == brojac.length - 1) if(obj[obj.length - 1].naziv == naziv){
                                console.log("udhg");
                                const editedText = sviPredmeti.replace(`${obj[brojac[k]].naziv},${obj[brojac[k]].tip},${obj[brojac[k]].pocetak},${obj[brojac[k]].kraj},${obj[brojac[k]].dan}`,'');
                                var zavrsni = editedText.slice(0, -1);
                            }
                        }
                    }
                else {
                    const editedText = sviPredmeti.replace(`${obj[obj.length - 1].naziv},${obj[obj.length - 1].tip},${obj[obj.length - 1].pocetak},${obj[obj.length - 1].kraj},${obj[obj.length - 1].dan}`,'');
                    var zavrsni = editedText.slice(0, -1);
                }
                console.log("1" + zavrsni);
                fs.writeFileSync('aktivnosti.txt', zavrsni, 'utf-8');
                res.json({message:"Uspješno obrisana aktivnost!"});
        }
    });
        
});

app.delete('/v1/predmet/:naziv', function (req, res) { 
        var naziv = req.params.naziv;
        fs.readFile('predmeti.txt', function read(err, data) {
            if (err) {
                throw err;
            }
            let imaPredmet = 0;
            var obj = pojednostavi(data.toString());
            for(var i = 0; i < obj.length; i++){
                if(obj[i].naziv == naziv) {
                imaPredmet = 1;
                }
            }
            let zadnji = 0;
            if(obj[obj.length - 1].naziv == naziv) zadnji = 1;
            console.log(obj[obj.length - 1].naziv + "hejjjj");
            if(imaPredmet != 1)  res.json({message:"Greška - predmet nije obrisan!"});
            else{
                var sviPredmeti = fs.readFileSync('predmeti.txt', 'utf-8');
                if(zadnji != 1)  var zavrsni = sviPredmeti.replace(`${naziv}\n`,'');
                else {
                    const editedText = sviPredmeti.replace(`${naziv}`,'');
                    var zavrsni = editedText.slice(0, -1);
                }
                fs.writeFileSync('predmeti.txt', zavrsni, 'utf-8');
                res.json({message:"Uspješno obrisan predmet!"});
            }
        });
            
    });

app.delete('/v1/all', function (req, res) { 
    let obrisano = 0;
    fs.readFile('aktivnosti.txt', function read(err, data) {
            if (err) {
                throw err;
            }
            var obj = pojednostavi(data.toString());
            console.log(obj);
            var sviPredmeti = fs.readFileSync('aktivnosti.txt', 'utf-8');
            var zavrsni = sviPredmeti;
            if(obj.length != 0) {
                    for(var brojac = 0; brojac < obj.length - 1; brojac++){
                    sviPredmeti = sviPredmeti.replace(`${obj[brojac].naziv},${obj[brojac].tip},${obj[brojac].pocetak},${obj[brojac].kraj},${obj[brojac].dan}\n`,'');
                    }
                const editedText = sviPredmeti.replace(`${obj[obj.length - 1].naziv},${obj[obj.length - 1].tip},${obj[brojac].pocetak},${obj[obj.length - 1].kraj},${obj[obj.length - 1].dan}`,'');
                zavrsni = editedText.slice(0, -1);
            }
            fs.writeFileSync('aktivnosti.txt', zavrsni, 'utf-8');
    });
    fs.readFile('predmeti.txt', function read(err, data) {
        if (err) {
            throw err;
        }
        var obj = pojednostavi(data.toString());
        console.log(obj);
        var sviPredmeti = fs.readFileSync('predmeti.txt', 'utf-8');
        var zavrsni = sviPredmeti;
        if(obj.length != 0) {
            for(var brojac = 0; brojac < obj.length - 1; brojac++){
                    sviPredmeti = sviPredmeti.replace(`${obj[brojac].naziv}\n`,'');
            }
            const editedText = sviPredmeti.replace(`${obj[obj.length - 1].naziv}`,'');
            zavrsni = editedText.slice(0, -1);
        }
        obrisano += 1;
        fs.writeFileSync('predmeti.txt', zavrsni, 'utf-8');
        res.json({message:"Uspješno obrisan sadržaj datoteka!"});
        });
});
function inicijalizacija()
{
    return new Promise(function (resolve, reject){
        db.Tip.create({
            naziv: 'Vjezbe'
        }).then (object =>{ 
        db.Dan.create({
            naziv: 'Ponedjeljak'
        }).then (object => {
            db.Dan.create({
                naziv: 'Utorak'
            }).then (object => {
                db.Dan.create({
                    naziv: 'Srijeda'
                }).then (object => {
                db.Student.create({
                    ime: 'Mujo Mujic',
                    index: '18000'
                }).then (object => {
                    db.Student.create({
                        ime: 'Niko Nikic',
                        index: '15888'
                    }).then (object => {
                        db.Predmet.create({
                            naziv: 'RMA'
                        }).then (object => {
                            db.Predmet.create({
                                naziv: 'OOAD'
                            }).then (object => {
                                db.Grupa.create({
                                    naziv: 'grupa1',
                                    predmet: 1
                                }).then (object => {
                                    db.Grupa.create({
                                        naziv: 'grupa2',
                                        predmet: 2
                                    }).then (object => {
                                    db.Aktivnost.create({
                                        naziv: 'RMApredavanje',
                                        pocetak: 12,
                                        kraj: 13,
                                        predmet: 1,
                                        grupa: 1,
                                        dan: 1,
                                        tip: 1
                                    });})});});});});});
        });});});});})
}
app.listen(3000);
module.exports = app; 
