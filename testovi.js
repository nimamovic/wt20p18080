let assert = chai.assert;
alert = function() {};
describe('Iscrtaj', function() {
 describe('iscrtajRaspored()', function() {
   it('Provjera dobrrog broja reodva', function() {
    document.getElementById('okvir').innerHTML = "";
    Iscrtaj.dodajDiv(document.getElementById('okvir'));
    Iscrtaj.dodajDane(['Ponedjeljak','Petak']);
    Iscrtaj.dodajSatPocetka(8);
    Iscrtaj.dodajSatKraj(21);
    Iscrtaj.iscrtajRaspored();
    let raspored = document.getElementById('okvir');
    let tabela = raspored.firstChild;
    let brojRedova = tabela.rows;
    assert.equal(brojRedova.length, 3,"Broj redova treba biti 3");
   });
   it('Provjera dobrog broja kolona prvog reda za standradnu satnicu', function() {
    document.getElementById('okvir').innerHTML = "";
    Iscrtaj.dodajDiv(document.getElementById('okvir'));
    Iscrtaj.dodajDane(['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak']);
    Iscrtaj.dodajSatPocetka(8);
    Iscrtaj.dodajSatKraj(21);
    Iscrtaj.iscrtajRaspored();
    let raspored = document.getElementById('okvir');
    let tabela = raspored.firstChild;
    let brojKolona = tabela.rows[0].cells;
    assert.equal(brojKolona.length,22,"Broj kolona prvog reda treba biti 22");
   });
   it('Provjera dobrog broja kolona reda dana za od 0 do 23', function() {
    document.getElementById('okvir').innerHTML = "";
    Iscrtaj.dodajDiv(document.getElementById('okvir'));
    Iscrtaj.dodajDane(['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak']);
    Iscrtaj.dodajSatPocetka(0);
    Iscrtaj.dodajSatKraj(23);
    Iscrtaj.iscrtajRaspored();
    let raspored = document.getElementById('okvir');
    let tabela = raspored.firstChild;
    let brojKolona = tabela.rows[1].cells;
    assert.equal(brojKolona.length, 47,"Broj kolona redova sa danima treba biti 47.");
   });
   it('Provjera postavljanja vremena koje se ne ispisuje.', function() {
    document.getElementById('okvir').innerHTML = "";
    Iscrtaj.dodajDiv(document.getElementById('okvir'));
    Iscrtaj.dodajDane(['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak']);
    Iscrtaj.dodajSatPocetka(9);
    Iscrtaj.dodajSatKraj(21);
    Iscrtaj.iscrtajRaspored();
    let raspored = document.getElementById('okvir');
    let tabela = raspored.firstChild;
    let ispisKolone = tabela.rows[0].cells[2].innerText;
    assert.equal(ispisKolone, "","Treba pisati prazan prostor.");
   });
   it('Provjera za od 8 do 8(isti početak i kraj, sto je "Greška".', function() {
    document.getElementById('okvir').innerHTML = "";
    Iscrtaj.dodajDiv(document.getElementById('okvir'));
    Iscrtaj.dodajDane(['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak']);
    Iscrtaj.dodajSatPocetka(8);
    Iscrtaj.dodajSatKraj(8);
    Iscrtaj.iscrtajRaspored();
    let raspored = document.getElementById('okvir');
    assert.equal(raspored.firstChild.innerHTML, "Greška","Treba pisati greška.");
   });
   it('Provjera dobrog broja kolona za od 9 do 8 sto je "Greška".', function() {
    document.getElementById('okvir').innerHTML = "";
    Iscrtaj.dodajDiv(document.getElementById('okvir'));
    Iscrtaj.dodajDane(['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak']);
    Iscrtaj.dodajSatPocetka(9);
    Iscrtaj.dodajSatKraj(8);
    Iscrtaj.iscrtajRaspored();
    let raspored = document.getElementById('okvir');
    assert.equal(raspored.firstChild.innerHTML, "Greška","Treba pisati greška.");
   });
   it('Provjera dobrog broja kolona za od -2(negativan) do 8 sto je "Greška".', function() {
    document.getElementById('okvir').innerHTML = "";
    Iscrtaj.dodajDiv(document.getElementById('okvir'));
    Iscrtaj.dodajDane(['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak']);
    Iscrtaj.dodajSatPocetka(-2);
    Iscrtaj.dodajSatKraj(8);
    Iscrtaj.iscrtajRaspored();
    let raspored = document.getElementById('okvir');
    assert.equal(raspored.firstChild.innerHTML, "Greška","Treba pisati greška.");
   });
   it('Provjera dobrog broja kolona za od 0 do 25(preko 24) sto je "Greška".', function() {
    document.getElementById('okvir').innerHTML = "";
    Iscrtaj.dodajDiv(document.getElementById('okvir'));
    Iscrtaj.dodajDane(['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak']);
    Iscrtaj.dodajSatPocetka(0);
    Iscrtaj.dodajSatKraj(25);
    Iscrtaj.iscrtajRaspored();
    let raspored = document.getElementById('okvir');
    assert.equal(raspored.firstChild.innerHTML, "Greška","Treba pisati greška.");
   });
   it('Provjera dobrog broja kolona za od 2.3(nije cijeli broj) do 8 sto je "Greška".', function() {
    document.getElementById('okvir').innerHTML = "";
    Iscrtaj.dodajDiv(document.getElementById('okvir'));
    Iscrtaj.dodajDane(['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak']);
    Iscrtaj.dodajSatPocetka(2.3);
    Iscrtaj.dodajSatKraj(8);
    Iscrtaj.iscrtajRaspored();
    let raspored = document.getElementById('okvir');
    assert.equal(raspored.firstChild.innerHTML, "Greška","Treba pisati greška.");
   });
   it('Provjera dobrog broja kolona za od tekst(vrijednost string koja nije podrzana) do 8 sto je "Greška".', function() {
    document.getElementById('okvir').innerHTML = "";
    Iscrtaj.dodajDiv(document.getElementById('okvir'));
    Iscrtaj.dodajDane(['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak']);
    Iscrtaj.dodajSatPocetka('tekst');
    Iscrtaj.dodajSatKraj(8);
    Iscrtaj.iscrtajRaspored();
    let raspored = document.getElementById('okvir');
    assert.equal(raspored.firstChild.innerHTML, "Greška","Treba pisati greška.");
   });
   it('Provjera dobrog broja kolona za od 26 do -2(nijedna vrijendost nije ispravna) sto je "Greška".', function() {
    document.getElementById('okvir').innerHTML = "";
    Iscrtaj.dodajDiv(document.getElementById('okvir'));
    Iscrtaj.dodajDane(['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak']);
    Iscrtaj.dodajSatPocetka(26);
    Iscrtaj.dodajSatKraj(-2);
    Iscrtaj.iscrtajRaspored();
    let raspored = document.getElementById('okvir');
    assert.equal(raspored.firstChild.innerHTML, "Greška","Treba pisati greška.");
   });
 });
  describe('dodajAktivnost()', function() {
    it('Provjera da li je dan dodat u isprvan red, tako sto je smanjen broj kolona tog reda', function() {
        document.getElementById('okvir').innerHTML = "";
        Iscrtaj.dodajDiv(document.getElementById('okvir'));
        Iscrtaj.dodajDane(['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak']);
        Iscrtaj.dodajSatPocetka(8);
        Iscrtaj.dodajSatKraj(21);
        Iscrtaj.iscrtajRaspored();
        Iscrtaj.dodajRaspored(document.getElementById('okvir'));
        Iscrtaj.dodajNaziv('WT');
        Iscrtaj.dodajTip('predavanje');
        Iscrtaj.dodajVrijemePocetak(14.5);
        Iscrtaj.dodajVrijemeKraj(16);
        Iscrtaj.dodajDan('Srijeda');
        Iscrtaj.dodajAktivnost();
        let raspored = document.getElementById('okvir');
        let tabela = raspored.firstChild;
        let trazeniRed = tabela.rows[3].cells;
        assert.equal(trazeniRed.length, 25,"Broj redova treba biti 25");
    });
    it('Provjera da li je dodata aktivnost u pravu celiju', function() {
        document.getElementById('okvir').innerHTML = "";
        Iscrtaj.dodajDiv(document.getElementById('okvir'));
        Iscrtaj.dodajDane(['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak']);
        Iscrtaj.dodajSatPocetka(8);
        Iscrtaj.dodajSatKraj(21);
        Iscrtaj.iscrtajRaspored();
        Iscrtaj.dodajRaspored(document.getElementById('okvir'));
        Iscrtaj.dodajNaziv('WT');
        Iscrtaj.dodajTip('predavanje');
        Iscrtaj.dodajVrijemePocetak(14.5);
        Iscrtaj.dodajVrijemeKraj(16);
        Iscrtaj.dodajDan('Srijeda');
        Iscrtaj.dodajAktivnost();
        let raspored = document.getElementById('okvir');
        let tabela = raspored.firstChild;
        let trazenaCelija = tabela.rows[3].cells[14];
        alert = function() {};
        assert.equal(trazenaCelija.innerText , 'WT\npredavanje' ,"U celiji treba pisati  WT predavanje");
    });
    it('Provjera da li je se moze dodati vrijeme od 17 do 16.5 - treba biti greška', function(done) {
        this.timeout(10000);
        document.getElementById('okvir').innerHTML = "";
        Iscrtaj.dodajDiv(document.getElementById('okvir'));
        Iscrtaj.dodajDane(['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak']);
        Iscrtaj.dodajSatPocetka(8);
        Iscrtaj.dodajSatKraj(21);
        Iscrtaj.iscrtajRaspored();
        Iscrtaj.dodajRaspored(document.getElementById('okvir'));
        Iscrtaj.dodajNaziv('WT');
        Iscrtaj.dodajTip('predavanje');
        Iscrtaj.dodajVrijemePocetak(17);
        Iscrtaj.dodajVrijemeKraj(16.5);
        Iscrtaj.dodajDan('Srijeda');
        let tekst = Iscrtaj.dodajAktivnost();
        assert.equal(tekst,"Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin","Treba ispisati grešku.");
        done();
    });
    it('Provjera da li pocetak vremena moze biti 12.2', function(done) {
      this.timeout(10000);
      document.getElementById('okvir').innerHTML = "";
      Iscrtaj.dodajDiv(document.getElementById('okvir'));
      Iscrtaj.dodajDane(['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak']);
      Iscrtaj.dodajSatPocetka(8);
      Iscrtaj.dodajSatKraj(21);
      Iscrtaj.iscrtajRaspored();
      Iscrtaj.dodajRaspored(document.getElementById('okvir'));
      Iscrtaj.dodajNaziv('WT');
      Iscrtaj.dodajTip('predavanje');
      Iscrtaj.dodajVrijemePocetak(12.2);
      Iscrtaj.dodajVrijemeKraj(16.5);
      Iscrtaj.dodajDan('Srijeda');
      let tekst = Iscrtaj.dodajAktivnost();
      assert.equal(tekst,"Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin","Treba ispisati grešku.");
      done();
  });
  it('Provjera da li je raspored undefined', function(done) {
    this.timeout(10000);
    document.getElementById('okvir').innerHTML = "";
    let tekst = Iscrtaj.dodajAktivnost();
    assert.equal(tekst,"Greška - raspored nije kreiran","Treba ispisati grešku.");
    done();
  });
  it('Dodavanje aktivnosti prije satnice koja piše, jer i počinje raspored od vrijednosti koja je prije napisane.', function(done) {
    this.timeout(10000);
    document.getElementById('okvir').innerHTML = "";
    Iscrtaj.dodajDiv(document.getElementById('okvir'));
    Iscrtaj.dodajDane(['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak']);
    Iscrtaj.dodajSatPocetka(9);
    Iscrtaj.dodajSatKraj(21);
    Iscrtaj.iscrtajRaspored();
    Iscrtaj.dodajRaspored(document.getElementById('okvir'));
    Iscrtaj.dodajNaziv('WT');
    Iscrtaj.dodajTip('predavanje');
    Iscrtaj.dodajVrijemePocetak(9.5);
    Iscrtaj.dodajVrijemeKraj(12);
    Iscrtaj.dodajDan('Srijeda');
    Iscrtaj.dodajAktivnost();
    let raspored = document.getElementById('okvir');
    let tabela = raspored.firstChild;
    let trazenaCelija= tabela.rows[3].cells[2].innerText;
    assert.equal(trazenaCelija,'WT\npredavanje');
    done();
  }); 
  it('Dodavanje aktivnosti prije satnice koja piše i završavanje prije, jer i počinje raspored od vrijednosti koja je prije napisane.', function(done) {
    this.timeout(10000);
    document.getElementById('okvir').innerHTML = "";
    Iscrtaj.dodajDiv(document.getElementById('okvir'));
    Iscrtaj.dodajDane(['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak']);
    Iscrtaj.dodajSatPocetka(13);
    Iscrtaj.dodajSatKraj(21);
    Iscrtaj.iscrtajRaspored();
    Iscrtaj.dodajRaspored(document.getElementById('okvir'));
    Iscrtaj.dodajNaziv('WT');
    Iscrtaj.dodajTip('predavanje');
    Iscrtaj.dodajVrijemePocetak(13.5);
    Iscrtaj.dodajVrijemeKraj(14.5);
    Iscrtaj.dodajDan('Srijeda');
    Iscrtaj.dodajAktivnost();
    let raspored = document.getElementById('okvir');
    let tabela = raspored.firstChild;
    let trazenaCelija= tabela.rows[3].cells[2].innerText;
    assert.equal(trazenaCelija,'WT\npredavanje');
    done();
  }); 
  it('Dodavanje aktivnosti koja počinje prije i završava poslije već dodane aktivnosti.', function(done) {
    this.timeout(10000);
    document.getElementById('okvir').innerHTML = "";
    Iscrtaj.dodajDiv(document.getElementById('okvir'));
    Iscrtaj.dodajDane(['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak']);
    Iscrtaj.dodajSatPocetka(13);
    Iscrtaj.dodajSatKraj(21);
    Iscrtaj.iscrtajRaspored();
    Iscrtaj.dodajRaspored(document.getElementById('okvir'));
    Iscrtaj.dodajNaziv('WT');
    Iscrtaj.dodajTip('predavanje');
    Iscrtaj.dodajVrijemePocetak(14);
    Iscrtaj.dodajVrijemeKraj(16);
    Iscrtaj.dodajDan('Srijeda');
    Iscrtaj.dodajAktivnost();
    Iscrtaj.dodajRaspored(document.getElementById('okvir'));
    Iscrtaj.dodajNaziv('WT');
    Iscrtaj.dodajTip('vjezbe');
    Iscrtaj.dodajVrijemePocetak(13);
    Iscrtaj.dodajVrijemeKraj(17);
    Iscrtaj.dodajDan('Srijeda');
    let tekst = Iscrtaj.dodajAktivnost();
    assert.equal(tekst,"Greška - već postoji termin u rasporedu u zadanom vremenu");
    done();
  }); 
  it('Dodavanje aktivnosti na dan koji ne postoji.', function(done) {
    this.timeout(10000);
    document.getElementById('okvir').innerHTML = "";
    Iscrtaj.dodajDiv(document.getElementById('okvir'));
    Iscrtaj.dodajDane(['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak']);
    Iscrtaj.dodajSatPocetka(13);
    Iscrtaj.dodajSatKraj(21);
    Iscrtaj.iscrtajRaspored();
    Iscrtaj.dodajRaspored(document.getElementById('okvir'));
    Iscrtaj.dodajNaziv('WT');
    Iscrtaj.dodajTip('predavanje');
    Iscrtaj.dodajVrijemePocetak(13);
    Iscrtaj.dodajVrijemeKraj(16);
    Iscrtaj.dodajDan('Subota');
    let tekst = Iscrtaj.dodajAktivnost();
    assert.equal(tekst,"Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin");
    done();
  });
  it('Dodavanje aktivnosti u vrijeme prije početnog vremena u rasporedu.', function(done) {
    this.timeout(10000);
    document.getElementById('okvir').innerHTML = "";
    Iscrtaj.dodajDiv(document.getElementById('okvir'));
    Iscrtaj.dodajDane(['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak']);
    Iscrtaj.dodajSatPocetka(14);
    Iscrtaj.dodajSatKraj(21);
    Iscrtaj.iscrtajRaspored();
    Iscrtaj.dodajRaspored(document.getElementById('okvir'));
    Iscrtaj.dodajNaziv('WT');
    Iscrtaj.dodajTip('predavanje');
    Iscrtaj.dodajVrijemePocetak(13);
    Iscrtaj.dodajVrijemeKraj(16);
    Iscrtaj.dodajDan('Srijeda');
    let tekst = Iscrtaj.dodajAktivnost();
    assert.equal(tekst,"Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin");
    done();
  }); 
  it('Dodavanje druge aktivnosti u isto vrijeme kad je i prva.', function(done) {
    this.timeout(10000);
    document.getElementById('okvir').innerHTML = "";
    Iscrtaj.dodajDiv(document.getElementById('okvir'));
    Iscrtaj.dodajDane(['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak']);
    Iscrtaj.dodajSatPocetka(9);
    Iscrtaj.dodajSatKraj(21);
    Iscrtaj.iscrtajRaspored();
    Iscrtaj.dodajRaspored(document.getElementById('okvir'));
    Iscrtaj.dodajNaziv('WT');
    Iscrtaj.dodajTip('predavanje');
    Iscrtaj.dodajVrijemePocetak(10.5);
    Iscrtaj.dodajVrijemeKraj(11);
    Iscrtaj.dodajDan('Srijeda');
    Iscrtaj.dodajAktivnost();
    Iscrtaj.dodajRaspored(document.getElementById('okvir'));
    Iscrtaj.dodajNaziv('WT');
    Iscrtaj.dodajTip('vjezbe');
    Iscrtaj.dodajVrijemePocetak(10.5);
    Iscrtaj.dodajVrijemeKraj(11);
    Iscrtaj.dodajDan('Srijeda');
    let tekst = Iscrtaj.dodajAktivnost();
    assert.equal(tekst,"Greška - već postoji termin u rasporedu u zadanom vremenu");
    done();
  }); 
  });
});
/*Iscrtaj.dodajRaspored(document.getElementById('okvir'));
    Iscrtaj.dodajNaziv('WT');
    Iscrtaj.dodajTip('predavanje');
    Iscrtaj.dodajVrijemePocetak(14.5);
    Iscrtaj.dodajVrijemeKraj(16);
    Iscrtaj.dodajDan('Srijeda');
    Iscrtaj.dodajAktivnost();
    
        */