const Sequelize = require("sequelize");
const sequelize = new Sequelize("wt2018080","root","root",{host:"127.0.0.1",dialect:"mysql",logging:false, port:3306});
const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;

db.Predmet = sequelize.import(__dirname+'/modeli/predmet.js');
db.Grupa = sequelize.import(__dirname+'/modeli/grupa.js');
db.Aktivnost = sequelize.import(__dirname+'/modeli/aktivnost.js');
db.Dan = sequelize.import(__dirname+'/modeli/dan.js');
db.Tip = sequelize.import(__dirname+'/modeli/tip.js');
db.Student = sequelize.import(__dirname+'/modeli/student.js');
//Predmet 1-N Grupa
db.Predmet.hasMany(db.Grupa,{foreignKey: { name: 'predmet', allowNull: false }});
db.Grupa.belongsTo(db.Predmet, {foreignKey : { name: 'predmet', allowNull: false }});

//Aktivnost N-1 Predmet znaci obrnuto 1-n 
db.Predmet.hasMany(db.Aktivnost,{foreignKey: { name: 'predmet', allowNull: false }});
db.Aktivnost.belongsTo(db.Predmet, {foreignKey : { name: 'predmet',  allowNull: false }});

//Aktivnost N-0 Grupa ovaj ne znam i ne kontam ni kako ide
db.Grupa.hasMany(db.Aktivnost, {foreignKey :  { name: 'grupa', allowNull: true }});
db.Aktivnost.belongsTo(db.Grupa, { foreignKey: { name: 'grupa',   allowNull: true }}); //mozda je to to??

//Aktivnost N-1 Dan znaci obrnuo 1-n
db.Dan.hasMany(db.Aktivnost,{foreignKey: { name: 'dan', allowNull: false }});
db.Aktivnost.belongsTo(db.Dan, {foreignKey : { name: 'dan',  allowNull: false }});

//Aktivnost N-1 Tip
db.Tip.hasMany(db.Aktivnost,{foreignKey: { name: 'tip', allowNull: false }});
db.Aktivnost.belongsTo(db.Tip, {foreignKey : { name: 'tip',   allowNull: false }});

//Student N-M Grupa
db.StudentGrupa = db.Grupa.belongsToMany(db.Student,{as:'studenti',through:'student_grupa',foreignKey:'grupaId'});
db.Student.belongsToMany(db.Grupa,{as:'grupe',through:'student_grupa',foreignKey:'studentId'});

module.exports=db;