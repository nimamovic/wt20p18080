let listaAktivnosti = [];
let listaPredmeta = [];
let uspjesnoDodanPredmet = 0;
let uspjesnoDodanaAktivnost = 0;


//treca spirala
/*
function ucitajAktivnosti(){  
    listaAktivnosti = [];
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "http://localhost:3000/v1/aktivnosti", true);
    xhttp.onreadystatechange = function() {
        if(xhttp.readyState === 4)
        {
            if(xhttp.status === 200 || xhttp.status == 0)
            {
                let data = JSON.parse(xhttp.responseText);
                $.each(data, function(){
                var akt = {naziv:this['naziv'],tip:this['tip'],pocetak:this['pocetak'],kraj:this['kraj'],dan:this['dan']};
                listaAktivnosti.push(akt);
                });
                console.log(listaAktivnosti);
             }
        }
    };
    xhttp.send();
    return listaAktivnosti;
 }
function ucitajPredmete(){  
    listaPredmeta = [];
    var xhttp2 = new XMLHttpRequest();
    xhttp2.open("GET", "http://localhost:3000/v1/predmeti", true);
    xhttp2.onreadystatechange = function() {
        if(xhttp2.readyState === 4)
        {
            if(xhttp2.status === 200 || xhttp2.status == 0)
            {
                let data = JSON.parse(xhttp2.responseText);
                $.each(data, function(){
                var predmet = {naziv:this['naziv']};
                listaPredmeta.push(predmet);
                });
                console.log(listaPredmeta);
             }
        }
    };
    xhttp2.send();
    return listaPredmeta;
 }

function potvrdaUnosa(){
    uspjesnoDodanPredmet = 0;
    let nazivPredmeta = document.getElementById("fname").value;
    let imaPredmet = 0;
    for(var i = 0; i < listaPredmeta.length; i++){
        if(listaPredmeta[i].naziv == nazivPredmeta) imaPredmet = 1;
    }
    let posalji = {naziv:nazivPredmeta};
    if(imaPredmet == 0) 
    {
        dodajPredmet(posalji);
    }
    let tip = document.getElementById("lista").value;
    let pocetak = document.getElementById("vpoc").value;
    let kraj = document.getElementById("vkraj").value;
    let dan = document.getElementById("dani").value;
    pocetak = pocetak.replace(":00","");
    pocetak = pocetak.replace(":30",".5");
    pocetak = pocetak.replace("10","1x");
    pocetak = pocetak.replace("0","");
    pocetak = pocetak.replace("1x","10");
    kraj = kraj.replace(":00","");
    kraj = kraj.replace(":30",".5");
    kraj = kraj.replace("10","1x");
    kraj = kraj.replace("0","");
    kraj = kraj.replace("1x","10");
    var akt = {naziv:nazivPredmeta,tip:tip,pocetak:pocetak,kraj:kraj,dan:dan};
    dodajAktivnost(akt,imaPredmet);    
}

 function dodajPredmet(predmet){
    let file = new XMLHttpRequest(); 
    file.open("POST", "//localhost:3000/v1/predmet", true);
    file.setRequestHeader("Content-Type", "application/json");
    file.send(JSON.stringify(predmet));
 }
 function dodajAktivnost(aktivnost,ima){
    let file = new XMLHttpRequest(); 
    file.open("POST", "//localhost:3000/v1/aktivnost", true);
    file.setRequestHeader("Content-Type", "application/json");
    file.send(JSON.stringify(aktivnost));
    file.onreadystatechange = function () {
        if (file.readyState == 4 && file.status == 200) {
            let jsonText = JSON.parse(file.responseText);
            console.log(jsonText);
            if(jsonText.message == "Aktivnost nije validna!" && ima != 1){
                console.log("udje");
                brisiPredmet(aktivnost.naziv);
            }
        }
    }
 }
 function brisiPredmet(predmet){
     console.log("evo me");
    let file = new XMLHttpRequest(); 
    file.open("DELETE", "//localhost:3000/v1/predmet/" + predmet, true);
    file.send(JSON.stringify(predmet));
 }*/

 function ucitajTip(){  
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "http://localhost:3000/v2/tip", true);
    xhttp.onreadystatechange = function() {
        if(xhttp.readyState === 4)
        {
            if(xhttp.status === 200 || xhttp.status == 0)
            {
                let data = JSON.parse(xhttp.responseText);
                $.each(data, function(){
                var tip = {naziv:this['naziv']};
                var x = document.getElementById("lista");
                var option = document.createElement("option");
                option.text = tip.naziv;
                x.add(option);
                });
             }
        }
    };
    xhttp.send();
 }
 function ucitajDane(){  
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "http://localhost:3000/v2/dan", true);
    xhttp.onreadystatechange = function() {
        if(xhttp.readyState === 4)
        {
            if(xhttp.status === 200 || xhttp.status == 0)
            {
                let data = JSON.parse(xhttp.responseText);
                $.each(data, function(){
                var dan = {naziv:this['naziv']};
                var x = document.getElementById("dani");
                var option = document.createElement("option");
                option.text = dan.naziv;
                x.add(option);
                });
             }
        }
    };
    xhttp.send();
 }

 function klik(){
    let nazivPredmeta = document.getElementById("fname").value;
    let tip = document.getElementById("lista").value;
    let pocetak = document.getElementById("vpoc").value;
    let kraj = document.getElementById("vkraj").value;
    let dan = document.getElementById("dani").value;
    var akt = {naziv:nazivPredmeta,tip:tip,pocetak:pocetak,kraj:kraj,dan:dan};
    dodajAktivnost(akt);   
 }
 function dodajAktivnost(aktivnost){
    let file = new XMLHttpRequest(); 
    file.open("POST", "//localhost:3000/v2/aktivnost/unos", true);
    file.setRequestHeader("Content-Type", "application/json");
    file.send(JSON.stringify(aktivnost));
    file.onreadystatechange = function () {
        if (file.readyState == 4 && file.status == 200) {
            let jsonText = JSON.parse(file.responseText);
            console.log(jsonText);
        }
    }
 }