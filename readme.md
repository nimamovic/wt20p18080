First step:
```
npm install
```

Start the app with
```
node index.js
```

Start tests with
```
npm test
```

Create empty database:
```
node kreirajBazu.js
```