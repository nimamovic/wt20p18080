//var div = document.getElementById('okvir');
var Iscrtaj = (function(){
    var vrijemeDo;
    var satPocetak;
    var satKraj;
    var dani = [];
    var div;
    var raspored;
    var naziv;
    var tip;
    var vrijemePocetak;
    var vrijemeKraj;
    var dan;
    var iscrtajRaspored = function(){
        vrijemeDo = satKraj;
        if (satPocetak >= satKraj || satKraj > 24 || satPocetak < 0 || satPocetak > 24 || satKraj < 0 || !Number.isInteger(satKraj) || !Number.isInteger(satPocetak)) {
            var x = document.createElement("pre");
            var t = document.createTextNode("Greška");
            x.appendChild(t);
            div.appendChild(x);
            return;
        }
        var table = document.createElement("table");
        table.className="rtabela";
        var thead = document.createElement("thead");
        var tbody = document.createElement("tbody");
        var headRow = document.createElement("tr");
    
        var poceni = 0;
        var zavrsi = 0;
        var brojacCelija = 0;
        for(var i = 0; i < 2 ;i++){
            var th=document.createElement("th");
            headRow.appendChild(th);
            th.appendChild(document.createTextNode(" "));
        }
        brojacCelija++;
        var predhodni = 0;
        var izmedju = 0;
        var brZaIzmejdu = 0;
        [0,2,4,6,8,10,12,15,17,19,21,23].forEach(function(el) {
            if(satPocetak < el && satPocetak > predhodni){
                poceni = 1;
                var th = document.createElement("th");
                th.colSpan = "2";
                headRow.appendChild(th);
                th.appendChild(document.createTextNode(" "));
                brojacCelija += 2;
                var brojKolona = 0;
                if(predhodni == 12 && satPocetak == 13) brojKolona = 2;
                for(var i = 0; i < brojKolona; i++){
                    var th=document.createElement("th");
                    headRow.appendChild(th);
                    th.appendChild(document.createTextNode(" "));
                    brojacCelija++;
                }
            }
            if(el == satPocetak){
                poceni = 1;
    
            }
            if(el == satKraj){
                zavrsi = 1;
                brojacCelija--;
            }
            if(el > satKraj && satKraj > predhodni){
                zavrsi = 1;
                brojacCelija -= (el - satKraj)*2;
                brojacCelija--;
            }
            if(poceni == 1 && zavrsi == 0){
                var th = document.createElement("th");
                th.colSpan = "2";
                headRow.appendChild(th);
                th.appendChild(document.createTextNode(brojUSate(el)));
                brojacCelija += 2;
                var brojKolona = 2;
                if(izmedju == 1 && brZaIzmejdu == 0) brojKolona = 0;
                if(izmedju == 1 && brZaIzmejdu == 2) brojKolona = 2;
                if(el == 12){
                    brojKolona = 4;
                }
                for(var i = 0; i < brojKolona; i++){
                    var th=document.createElement("th");
                    headRow.appendChild(th);
                    th.appendChild(document.createTextNode(" "));
                    brojacCelija++;
                }
            }
            predhodni = el;
          });
    
        thead.appendChild(headRow);
        table.appendChild(thead); 
        dani.forEach(function(el) {
            var tr = document.createElement("tr");
            var td = document.createElement("td");
            var x = document.createElement("pre");
            var t = document.createTextNode(el + " ");
            x.appendChild(t);
            td.appendChild(x);
            td.colSpan = "3";
            tr.appendChild(td);
            for(var i = 0; i < brojacCelija; i++){
                var td = document.createElement("td");
                td.appendChild(document.createTextNode(" "));
                tr.appendChild(td);
            }
            tbody.appendChild(tr);  
        });
    
        table.appendChild(tbody);             
        div.appendChild(table);
    }
    var dodajAktivnost = function(){
        imaAlert(0);
        if(typeof raspored.firstChild === 'undefined'){
            alert("Greška - raspored nije kreiran")
            return "Greška - raspored nije kreiran";
        }
        if(raspored.firstChild == null) {
            alert("Greška - raspored nije kreiran");
            return "Greška - raspored nije kreiran";
        }
        if (vrijemePocetak > vrijemeKraj || vrijemePocetak*2 != parseInt(vrijemePocetak*2) || vrijemeKraj*2 != parseInt(vrijemeKraj*2)){
            alert("Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin")
            return "Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin";
        }
        
        var tabela = raspored.firstChild;
        var prob = tabela.rows[0].cells[2].innerText.replace(/\D/g, "");
        var m = 0;
        while(prob == ""){
            prob = tabela.rows[0].cells[m+2].innerText.replace(/\D/g, "");
            m++;
        }
        prob /= 100;
        if(m != 0) {
            prob = prob - 0.5 * m;
        }
        
        if(vrijemePocetak < prob || vrijemeKraj < prob) {
            alert("Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin")
            return "Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin";
        }
       if(vrijemeDo < vrijemeKraj){
        alert("Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin")
        return "Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin";
       }

        var red = 0;
        var imaLiDan = -1;
        for (var i = 1, row; row = tabela.rows[i]; i++) {
            if(row.cells[0].innerText == dan  + " "){
                red = i;
                imaLiDan = 1;
            } 
         }
        if(imaLiDan == -1){
            alert("Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin");
            return "Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin";
        }
        var pred = 0;
        var celijaPocetka = 0;
        var celijaKraja = 0;
        var pocetakNadjen = 0;
        var pom = 0;
        for(var j = 0, col; col = tabela.rows[0].cells[j]; j++) {
            pred = 0;
            var res = col.innerText.replace(/\D/g, "");
            res /= 100;
            console.log(res + "sad");
            if(tabela.rows[0].cells[2].innerText.replace(/\D/g, "") == "" && vrijemePocetak < res && pocetakNadjen == 0){
                pom = (res - vrijemePocetak) / 0.5;
                console.log("tusuuusu");
                if(res != 15) res = res - 2;
                else res = 12;
                pred = 1;
                celijaPocetka = j - pom;
                celijaPocetka++;
                console.log(celijaPocetka + "evveveve");
                pocetakNadjen = 1;
            }
            if(res != 0) 
            {
                celijaKraja++;
                if(pocetakNadjen == 0) celijaPocetka++;
            }
            if(res == vrijemePocetak && pocetakNadjen == 0){
                celijaPocetka += j;
                pocetakNadjen = 1;
            }
            else if(res + 0.5 == vrijemePocetak && pocetakNadjen == 0){
                celijaPocetka += j + 1;
                pocetakNadjen = 1;
            }
            else if(res + 1 == vrijemePocetak && pocetakNadjen == 0){
                celijaPocetka += j + 2;
                pocetakNadjen = 1;
            }
            else if(res + 1.5 == vrijemePocetak && pocetakNadjen == 0){
                celijaPocetka += j + 3;
                pocetakNadjen = 1;
                console.log("uslooo");
            }
            else if(res == 12 && res + 2 == vrijemePocetak && pocetakNadjen == 0){
                celijaPocetka += j + 4;
                pocetakNadjen = 1;
            }
            else if(res == 12 && res + 2.5 == vrijemePocetak && pocetakNadjen == 0){
                celijaPocetka += j + 5;
                pocetakNadjen = 1;
            }
            else if(tabela.rows[0].cells.length - 1 == j && pocetakNadjen == 0){
                celijaPocetka += j;
                pocetakNadjen = 1;
            }
            if(pred == 1){
                if(res < vrijemeKraj){
                    res += 2;
                    if(res == 14) res += 1;
                    if(res > vrijemeKraj){
                        pom = (res - vrijemeKraj) / 0.5;
                        res -= 2;
                        if(res == 13) res -= 1;
                        celijaKraja = j - pom;
                        console.log(celijaKraja + "ooooe");
                        pom = -1;
                        break;
                    }
                }
                else {
                    res += 2;
                    if(res == 14) res += 1;
                }
                console.log(res + "res");
            }
            if(res == vrijemeKraj && pom != -1){
                celijaKraja += j;
                break;
            }
            else if(res + 0.5 == vrijemeKraj && pom != -1){
                celijaKraja += j + 1;
                console.log("uslooo");
                break;
            }
            else if(res + 1 == vrijemeKraj && pom != -1){
                celijaKraja += j + 2;
                break;
            }
            else if(res + 1.5 == vrijemeKraj && pom != -1){
                celijaKraja += j + 3;
                console.log("uslo");
                break;
            }
            else if(res == 12 && res + 2 == vrijemeKraj && pom != -1){
                celijaKraja += j + 4;
                console.log("tu cam");
                break;
            }
            else if(res == 12 && res + 2.5 == vrijemeKraj && pom != -1){
                celijaKraja += j + 5;
                break;
            }
            else if(tabela.rows[0].cells.length - 1 == j && pocetakNadjen == 1 && pom != -1){
                celijaKraja += j + 2;
                break;
            }
        }
        if(tabela.rows[0].cells[2].innerText.replace(/\D/g, "") == "") {
            console.log("udjem");
            celijaPocetka++;
            celijaKraja++;
        }
        celijaKraja -= 2;
        celijaPocetka -= 2;
        var bezCsP = celijaPocetka;
        var odabraniRed = tabela.rows[red];
        for(var j = 1, col; col = tabela.rows[red].cells[j]; j++){
            if(j < celijaPocetka){
                var brojColspena = col.colSpan;
                if(brojColspena != 1){
                    celijaPocetka -= brojColspena - 1;
                    celijaKraja -= brojColspena - 1;
                    if((bezCsP > celijaPocetka && j + brojColspena > bezCsP) || celijaKraja <= 2 || celijaPocetka <= 2){
                        /*celijaPocetka += brojColspena - 1;
                        celijaKraja += brojColspena -1;*/
                        alert("Greška - već postoji termin u rasporedu u zadanom vremenu");
                        return "Greška - već postoji termin u rasporedu u zadanom vremenu";
                    }
                }
            }
        }
        console.log(celijaPocetka);
        console.log(celijaKraja);

        if(odabraniRed.cells[celijaPocetka].innerText.replace(" ", "") != ""){
            alert("Greška - već postoji termin u rasporedu u zadanom vremenu");
                        return "Greška - već postoji termin u rasporedu u zadanom vremenu";
        }

        if(celijaPocetka + 1 != celijaKraja) {
            console.log(celijaPocetka + celijaKraja + "nnn");
        for(var j = celijaPocetka; j < celijaKraja; j++){
            if(tabela.rows[red].cells[j].colSpan != 1){
                alert("Greška - već postoji termin u rasporedu u zadanom vremenu");
                        return "Greška - već postoji termin u rasporedu u zadanom vremenu";
            }
        }
        odabraniRed.cells[celijaPocetka].colSpan = celijaKraja - celijaPocetka;
        odabraniRed.cells[celijaPocetka].innerText = naziv + "\n" + tip;
            for(var i = celijaKraja - 1; i > celijaPocetka ; i--){
            odabraniRed.deleteCell(i);
            //odabraniRed.cells[i].style.backgroundColor = "#dbe6ea";
            }  
        }
        else{
            odabraniRed.cells[celijaPocetka].innerText = naziv + "\n" + tip;
            odabraniRed.cells[celijaPocetka].style.backgroundColor = "#dbe6ea";
            console.log("udje");
        }
        var pom = 0;
        if(odabraniRed.cells[celijaPocetka].colSpan % 2 == 0 ){
            if(bezCsP % 2 == 1) odabraniRed.cells[celijaPocetka].style.border = "1px solid";
            else {
                odabraniRed.cells[celijaPocetka].style.borderLeft = "1px dashed";
                odabraniRed.cells[celijaPocetka].style.borderRight = "1px dashed";
            }
            for(var j = 1; j < celijaPocetka; j++){
                if(odabraniRed.cells[j].colSpan % 2 == 0) pom++;
            }
            for(var j = celijaPocetka + 1; j < odabraniRed.cells.length; j++){
                if((j + pom ) % 2 == 0) odabraniRed.cells[j].style.borderLeft = "1px solid";
                else odabraniRed.cells[j].style.borderLeft = "1px dashed";
                if(odabraniRed.cells[j].colSpan % 2 == 0) pom++; 
            }
        }
        return 1;
    }
    var dodajDiv = function(d){
        div = d;
    }
    var dodajSatPocetka = function(sP){
        satPocetak = sP;
    }
    var dodajSatKraj = function(skk){
        satKraj = skk;
    }
    var dodajDane = function(d){
        dani = d;
    }
    var dodajRaspored = function(r){
        raspored = r;
    }
    var dodajNaziv= function(n){
        naziv = n;
    }
    var dodajTip = function(t){
        tip = t;
    }
    var dodajVrijemePocetak = function(vP){
        vrijemePocetak = vP;
    }
    var dodajVrijemeKraj = function(vK){
        vrijemeKraj = vK;
    }
    var dodajDan = function(d){
        dan = d;
    }
    
    return {
        iscrtajRaspored: iscrtajRaspored,
        dodajDiv: dodajDiv,
        dodajSatPocetka: dodajSatPocetka,
        dodajSatKraj: dodajSatKraj,
        dodajDane: dodajDane,
        dodajRaspored: dodajRaspored,
        dodajNaziv: dodajNaziv,
        dodajTip: dodajTip,
        dodajVrijemePocetak: dodajVrijemePocetak,
        dodajVrijemeKraj: dodajVrijemeKraj,
        dodajDan: dodajDan,
        dodajAktivnost: dodajAktivnost,
    }
}());
/*Iscrtaj.dodajDiv(document.getElementById('okvir'));
Iscrtaj.dodajDane(['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak']);
Iscrtaj.dodajSatPocetka(14);
Iscrtaj.dodajSatKraj(21);
Iscrtaj.iscrtajRaspored();
Iscrtaj.dodajRaspored(document.getElementById('okvir'));
Iscrtaj.dodajNaziv('WT');
Iscrtaj.dodajTip('predavanje');
Iscrtaj.dodajVrijemePocetak(13);
Iscrtaj.dodajVrijemeKraj(16);
Iscrtaj.dodajDan('Srijeda');
Iscrtaj.dodajAktivnost();*/
//let tekst = Iscrtaj.dodajAktivnost();//OVDJE MI SE TEK BACA ALERT JER DRUFAICJE CE SE PREDUGO IZVRSAVATI TEST NE ZNAM STA JE PAMETNIJE 
//if(tekst != 1) alert(tekst);


function brojUSate(el){
            var stringSati = "";
            if(el < 10) stringSati = '0';
            stringSati += el + ':00';
              return stringSati
}
function imaAlert(v){
    if(v != 0) return 1;
    return 0;
}